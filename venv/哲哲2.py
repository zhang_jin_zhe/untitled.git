"""
   project name：jinzhe's third time

   name:Zhang jin zhe

   student number:20193227

   time:2020.3.18
"""
print("王者荣耀中的坦克有：")
坦克 = ["苏烈","猪八戒","项羽"]
for item in 坦克:
    print(item)
print("王者荣耀中的射手有：")
射手 = ["虞姬","李元芳","后裔"]
for item in 射手:
    print(item)
print("王者荣耀中的法师有：")
法师 = ["诸葛亮","嫦娥","貂蝉"]
for item in 法师:
    print(item)
print("王者荣耀中的打野有：")
打野 = ["孙悟空","李白","裴擒虎"]
for item in 打野:
    print(item)
print("王者荣耀中的辅助有：")
辅助 = ["鬼谷子","庄周","鲁班大师"]
for item in 辅助:
    print(item)
辅助.insert(2,"孙膑") #像列表中添加元素
print("王者荣耀中的辅助有：",(辅助))
打野[1]="娜可露露"    #改变列表中的元素
print("王者荣耀中的打野有：",(打野))
del 法师[2]          #删除列表中的元素
print("王者荣耀中的法师有：",(法师))

movie = [
    ("霸王别姬",9.6),
    ("美丽人生",9.5),
    ("千与千寻",9.3),
    ("这个杀手不太冷",9.4),
    ("肖申克的救赎",9.7),
    ("忠犬八公的故事",9.4),
    ("盗梦空间",9.3),
    ("阿甘正传",9.5),
    ("泰坦尼克号",9.4),
    ("辛德勒的名单",9.5),
    ]
del movie[2]   #删除一个元素
movie.insert(2,("千与千寻",9.3))  #添加一个元素
movie=sorted(movie,key=lambda s:s[1],reverse=True)
print(movie)








