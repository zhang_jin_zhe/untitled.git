'''
文件内容：python综合实践

姓名：张金哲

学号：20193227

日期：2020.6.10
'''
import requests
import time
import re #Python正则表达式库

if __name__=='__main__':
     #海量爬取图片数据
     #进入网站(一般商业图片素材公司网站版权保护做得比较好,不容易爬取)
     #https://www.pexels.com/(该网站图片免费,易于爬取)
     #搜索关键词<man>,Chrome按下F12查看源码,发现图片链接
     url_picture='https://www.pexels.com/search/man/'
     response=requests.get(url=url_picture)
     with open('./pexels/man.html',mode='w',encoding='utf-8') as fp:
         fp.write(response.text)
         print('网页保存成功!')  #保存的html文件中含有多张图片的url地址


     #该正则获取小括号内内容 (.*?)  .表示任意字符,*表示匹配多个,?表示遇到"就停下来(非贪婪模式)
     num_name=1
     html=response.text
     pattern_url=r'<img srcset="(.*?)".*?>'  # r'':非转义的原始字符串
     pattern_img_name=r'pexels-photo-(.*?).jpeg'
     img_urls=re.findall(pattern_url,html) #得到的是一个list,里面是str元素,这些元素是匹配到的图片url
     print(img_urls)
     for img_url in img_urls:
         response=requests.get(img_url)
         content=response.content
         #img_name=re.findall(pattern_img_name,img_url) #该网站srcset内有两条可用的url,所以匹配后会有两条相同的name
         with open('./Pexels/'+str(num_name)+'.jpg','wb') as fp:
             fp.write(content)
             print(str(num_name)+'号图片下载成功!')
             num_name+=1
         time.sleep(0.1) #设置时间延迟 1s

     #Python 文件读写
     #open(文件地址,读写方式,编码方式),
     #读写方式:
     #文本'w'
     #图片'wb'