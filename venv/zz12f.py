'''
文件内容：python网络编程

姓名：张金哲

学号：20193227

日期：2020.5.23
'''
import socket
import os
import base64
os.chdir(r"D:\20193227张金哲")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8005))
s.listen()
conn, address = s.accept()
name = conn.recv(1024)
print("来自客户端的文件：", name.decode())
data = conn.recv(1024)
f = open("r.txt", "w")
data1 = base64.b32decode(data)
f.write(data1.decode())
f.close()
print("来自客户端的信息：", data.decode())
conn.sendall("已接收！".encode())
data1 = conn.recv(1024)
conn.sendall("reply.txt".encode())
f = open("r.txt", "r")
data1 = f.read()
conn.sendall(data1.encode())
f.close()
data1 = conn.recv(1024)
print("来自客户端的信息", data1.decode())
s.close()