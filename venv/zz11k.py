'''
文件内容：python网络编程

姓名：张金哲

学号：20193227

日期：2020.5.23
'''
import socket
import os
import base64
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8005))
str1 = input("请输入要传输的文件名：")
s.sendall(str1.encode())
os.chdir(r"D:\20193227张金哲")
file = open(str1, 'r')
text = file.read()
text = text.encode('utf-8')
encode_text = base64.b32encode(text)
s.sendall(encode_text)
file.close()
data = s.recv(1024)
print("来自客户端的信息：", data.decode())
s.sendall("收到".encode())
name = s.recv(1024)
print("来自客户端文件：", name.decode())
data = s.recv(1024)
f = open("reply.txt", "w")
f.write(data.decode())
f.close()
print("文件内容已发送")
s.sendall("已接收".encode())
s.close()