'''
文件内容：python网络编程

姓名：张金哲

学号：20193227

日期：2020.5.6
'''
import socket

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind(('127.0.0.1',8008))
s.listen()
conn,address = s.accept()
while True:
    data = conn.recv(1024)
    print(data.decode())
    conn.sendall(("已接受:"+str(data.decode())).encode())
s.close()