'''
文件内容：python数据库

姓名：张金哲

学号：20193227

日期：2020.4.29
'''
import os.path
import sqlite3

conn = sqlite3.connect("20193227.db")
cursor = conn.cursor()
cursor.execute('create table if not exists wangzhe (number int(10) primary key,name vachar(20))')
cursor.execute('insert into wangzhe (number, name ) values (1,"韩信")')
cursor.execute('insert into wangzhe (number, name ) values (2,"廉颇")')
cursor.execute('select * from wangzhe')
print(cursor.fetchall())

cursor.execute('update wangzhe set name = "赵云" where number= 2')
cursor.execute('select * from wangzhe')
print(cursor.fetchall())

cursor.execute('delete from wangzhe where number= 2')
cursor.execute('select * from wangzhe')
print(cursor.fetchall())

conn.close()


