'''
文件内容：python网络编程

姓名：张金哲

学号：20193227

日期：2020.5.6
'''
import socket
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect(('127.0.0.1',8008))
while True:
    str = input("已输入:")
    s.sendall(str.encode())
    data = s.recv(1024)
    print(data.decode())
s.close()