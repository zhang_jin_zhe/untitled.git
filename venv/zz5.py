'''
文件内容：python程序设计实验一

姓名：张金哲

学号：20193227

日期：2020.4.11
'''
def subtraction(a,b):            #定义一个作差函数，a，b为形参
    if a>b:
        result=a-b
    elif a<b:
        c=b
        b=a
        a=c
        result=a-b
    else :
        result=0
    return result
c="人生苦短，我用python！"
print("计算两个整数的差值。",c)     #输出字符串
a=int(input("请输入第一个整数："))  #从键盘输入，并将类型定为整型
b=int(input("请输入第二个整数："))
result=subtraction(a,b)           #使用作差函数，进行作差运算
print("两数的差值为：",result)

